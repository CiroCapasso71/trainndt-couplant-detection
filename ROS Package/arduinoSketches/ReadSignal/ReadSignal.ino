#include "config.h"
int inputSignal = 11;
int val = 0;


byte LO = 0x00;
byte HI = 0x01;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
   pinMode(inputSignal, INPUT);   
   //Serial.print("\nConsumer");    

}

void loop() {
  // put your main code here, to run repeatedly:

    val = digitalRead(inputSignal);  
  
    if(val == HIGH){ Serial.print(HI);}
  
    if(val == LOW) { Serial.print(LO);}

    delay(50);
   
}
